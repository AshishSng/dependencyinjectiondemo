﻿namespace DemoLibrary.Utilities
{
    public interface IDataAccess
    {
        void LoadData();
        void SavaData(string name);
    }
}